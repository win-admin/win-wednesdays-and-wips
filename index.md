---
layout: default
title: Home
nav_order: 1
description: ""
has_children: false
---

<!-- ![WIN-logo](docs/img/WIN-h100.png) -->

# WIN Wednesday and WIP Presentations
{: .fs-8 }

Find out how to book your slot in a WIN Wednesday session and prepare for a WIP
{: .fs-6 .fw-300 }

This repository is contains a guide to preparing for your Work in Progress (WIP) presentation, and how to book it using Calpendo. The calpendo guide is also useful for WIN Wednesday coordinators to book their seminars.

Have a look through the side bar to find out more about a WIP and book your slot.

![illustration of a women with dark skin facing an incomplete painting, a work in progress](img/undraw-wip.png)

This resource and guide was created by Andrew Galloway, Sebastian Rieger, Andrew Rudgewick-Brown and Cassandra Gould van Praag, incorporating feedback from the WIN Management Board.

You can follow the motivation, activities and decision making in the development of this resource in [this GitLab repository](https://git.fmrib.ox.ac.uk/win-admin/wip-update)

Illustrations by [undraw](https://undraw.co).
