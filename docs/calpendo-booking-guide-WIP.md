---
layout: default
title: WIP presenters
parent: Calpendo booking guide
has_children: false
nav_order: 1
---

# Guide for WIP Presenters
{: .fs-9 }

How to use Calpendo to book your WIP
{: .fs-6 .fw-300 }

---

**Contents**
- [What is a WIP](#what-is-a-wip)
- [How to book a WIP](#how-to-book-a-wip)
- [How to amend or cancel your WIP booking](#how-to-amend-or-cancel-your-wip-booking)
- [Approval of your booking](#approval-of-your-booking)
- [What if you need to change the date/time of your seminar?](#what-if-you-need-to-change-the-datetime-of-your-seminar)


## What is a WIP

Please see [this guide](../WIP-guide) for a description of what to expect from a WIP presentation, what to include, and where to get support.

## How to book a WIP

WIP presentations are advertised in the WIN Monday Message the week of your presentation. **It is therefore advisable to book your presentation by no later than 12:00 on Friday of the week proceeding your slot.**

### 1. Create an account

Ensure you have an active Calpendo account. This may take up to one week to review or reinstate. See [How do i get a Calpendo account](https://open.win.ox.ac.uk/pages/cassag/WIN-Wednesdays-and-WIPs/docs/calpendo-booking-guide/#how-do-i-get-a-calpendo-account).

### 2. Log into Calpendo using Shibboleth

Go to [http://calpendo.fmrib.ox.ac.uk/](http://calpendo.fmrib.ox.ac.uk/).

Click the "Shibboleth" button indicated below.

On the following page use your University of Oxford Shibboleth SSO to sign into the Calpendo homepage.

![calpendo sign in page with arrow highlighting Shibboleth sign in button](../../img/calpendo-shibboleth.png)

### 3. Select the WIN Wednesday resource

Select the WIN Wednesday resource from the list of available calendars.

Click the "Calendars" tab on the top left of the Calpendo page, then select WIN Wednesdays as shown below.

![calpendo booking calendar with arrows highlighting the calendar drop down and WIN Wednesday resource](../../img/calpendo-resource.png)

### 4. Find an available slot

Use the week navigation buttons near the top of the page to search through the calendar and find an available date for your presentation. Bookings are not allowed in the red shaded areas.

![calpendo booking calendar for WIN Wednesdays with arrows highlighting the week navigation buttons and an available slot](../../img/calpendo-available.png)

For any given week, there may be no pre-booked presentations, or there may be a seminar already scheduled. If there is a white space in the calendar, you are welcome to book it. This may mean that your presentation is held before or after another presentation.

### 5. Click to open the "new booking" window

Click in an available slot in the calendar to open the "new booking" window. Enter the required details as described below.

![calpendo new booking window for WIN Wednesdays with arrows highlighting: 1) end time; 2) email reminder notice period; 3) description text box; 4) Title, Authors and Abstract text box; 5) Recording consent upload button](../../img/calpendo-newbooking.png)

#### 1. Start and end time

As described in the [WIP Guide](../WIP-guide), WIP presentations are 20 minutes long = 12 minutes presentation + 8 minutes questions.

Ensure the start time of your WIP is 12:00. Adjust the end time to 12:20.

**Please note that the exact start time of each presentation within a WIN Wednesday session is subject to change, and we therefore request that speakers are present for the duration 12:00-13:00 the session.**

**We request that all presenters join the call 10 minutes before the start of the meeting to test audio-visual equipment with the host.**

#### 2. Reminder notice period

This sets the date which Calpendo will send you a reminder about your upcoming booking. This reminder will include the details of your booking as you have entered them. We suggest you use a **5 day** notice period, so you can review and amend your booking details before they are distributed in the Monday Message. See the process below to [amend a booking](#how-to-amend-or-cancel-your-wip-booking).

You can change the default reminder or notice period in your personal Calpendo settings. To do this, click "Settings" (top right) then "booking reminders" from the list on the left.

#### 3. Description

Please enter "WIP Presentation". This makes it easier for the WIN Wednesday Administrators to review your booking appropriately.

#### 4. Title, Authors and Abstract

Please enter the title of your project, the authors and an abstract. This information will be circulated in the Monday Message in the week of your presentation. **Bookings will not be approved until these details are complete.**

#### 5. Recording consent upload button

A signed speaker recording release ("Recording Consent") is required to record your WIP presentation. Recording allows other WIN members to review and provide feedback on your presentation.

Download and sign a [speaker recording release](https://help.it.ox.ac.uk/sites/default/files/help/documents/media/downloadable_legal_form_pack_3-10-22.zip) from Central IT. A signed release is required for everyone who intends to present in the main part of the meeting. A release is not required for people (for example Supervisors) who only intend to be on screen during the Q&A as this will not be recorded. Choose the MS Word .docx version, or .pdf if MS Word is not available. If using the .pdf, please create text boxes to write over the appropriate white spaces.

Once you have saved your signed release, click the "Choose File" button to locate the file and add it to your booking. **Bookings will not be approved until at least one signed release has been uploaded.**

#### 6. In-person or online speaker participation

To help us plan the meeting, we need to know whether the speaker will be present in-person from the [Cowey Room in the FMRIB Annex](https://www.win.ox.ac.uk/about/how-to-find-us/finding-win-fmrib/meeting-rooms), the [Meeting Room in OHBA](https://www.win.ox.ac.uk/about/how-to-find-us/finding-win-ohba), or joining the Teams call online.

Please identify where the speaker will be joining from by selecting from the drop down list. **Note the default is in-person - please update this if the speaker will be joining online.**

#### 7. Submit you booking

Click the "Create Booking" button to submit your booking for approval. Your booking will be sent to a WIN Wednesday administrator and reviewed for completeness before it is approved.

You will receive an email confirmation that your booking has been received. This email will be sent to the address associated with your Shibboleth account.

You will receive an email notification once your booking has been approved, or you will be asked for further information. **Your slot will be cancelled by an administrator if the booking information is not complete by 17:00 Friday of the week proceeding your slot.**

## How to amend or cancel your WIP booking

**Bookings can be amended by the booker up to 12:00 on the Friday before your scheduled presentation. If you need to amend your booking after that time, please [contact a WIN Wednesday administrator](../calpendo-booking-guide-support).**

To amend your booking, return to the WIN Wednesday resource calendar following steps [2. Log into Calpendo using Shibboleth](#2-log-into-calpendo-using-shibboleth) and [3. Select the WIN Wednesday resource](#3-select-the-win-wednesday-resource) above.

Edit the information as required and click "Update Booking" to save the changes. To cancel your booking, click "Cancel Booking". You will be notified that your booking has been cancelled.

![calpendo edit booking window for WIN Wednesdays with arrows highlighting the update and cancel booking buttons](../../img/calpendo-amend.png)


## Approval of your booking

You will get an email from Calpendo when your booking has been approved. Alternatively you may receive an email with an instruction to add or clarify some detail.

Once your booking has been approved you are still able to edit it, but we would prefer if you didn't! Please [contact an administrator](../calpendo-booking-guide-support) if you need to make any changes to your booking.

You booking will be cancelled if it is not complete by 17:00 on the Friday preceding your booked slot. You will receive and email notification if your booking is cancelled.

## What if you need to change the date/time of your seminar?

In some circumstances it may be necessary to hold your seminar outside of the usual Wednesday 12:00-13:00 slot, for example to meet the availability of external presenters. In these cases, please [contact a WIN Wednesday Administrators](../calpendo-booking-guide-support) and ask them to open a slot in Calpendo at the required time.
