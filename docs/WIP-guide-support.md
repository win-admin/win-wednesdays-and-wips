---
layout: default
title: Where to get help
parent: WIP guide
has_children: false
nav_order: 3
---

# Where to get help
{: .fs-9 }

Useful folks who are happy to help you prepare for your WIP
{: .fs-6 .fw-300 }

---

**Contents**
- [Core staff support (Local Promotors)](#core-staff-support-local-promotors)
- [Experimental set-up, task, equipment etc.](#experimental-set-up-task-equipment-etc)
- [MRI sequences that you will use](#mri-sequences-that-you-will-use)
- [Analysis plans](#analysis-plans)
- [Reproducibility and output sharing plans ("open science")](#reproducibility-and-output-sharing-plans-open-science)
- [Patient and Public Involvement & Engagement](#patient-and-public-involvement--engagement)
- [Ethics](#ethics)
- [Update this guidance](#update-this-guidance)

## Core staff support (Local Promotors)

The WIN Core staff named below are happy to assist you at any stage of planning your WIP. Please contact the one which is most appropriate for your type of research.

- Analysis: taylor.hanayik@ndcn.ox.ac.uk
- Physics: mohamed.tachrount@ndcn.ox.ac.uk or aaron.hess@ndcn.ox.ac.uk
- Preclinical: claire.bratley@ndcn.ox.ac.uk
- Clinical OHBA: clare.odonoghue@psych.ox.ac.uk
- Clinical FMRIB: jessica.walsh@ndcn.ox.ac.uk
- Clinical Neuerosciences: marieke.martens@psych.ox.ac.uk
- Cognitive Neuroscience (MRI): sebastian.rieger@psych.ox.ac.uk
- Cognitive Neuroscience (MEG/EEG): anna.camera@psych.ox.ac.uk
- All other themes or questions: cassandra.gouldvanpraag@psych.ox.ac.uk

## Experimental set-up, task, equipment etc.
If you have any questions about stimulus presentation equipment please get in touch with Seb (sebastian.rieger@psych.ox.ac.uk).

## MRI sequences that you will use
Please arrange to discuss your project with one of the radiographers before your WIP: radiographers@win.ox.ac.uk

If your project has more complex physics requirements please get in touch with Stuart (Stuart.Clare@ndcn.ox.ac.uk).

## Analysis plans
If you have any questions about analysis please contact Taylor (taylor.hanayik@ndcn.ox.ac.uk).

## Reproducibility and output sharing plans ("open science")
The outputs of your project are more than just a paper at the end. They include your experimental protocol, your data, your code and your tasks. We can help you turn these outputs into practical tools for you and your lab to efficiently reuse.

If you have any questions relating to open science practices, including how to use the [Open WIN infrastructure](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/), please contact Cassandra (cassandra.gouldvanpraag@psych.ox.ac.uk).

## Patient and Public Involvement & Engagement
PPI(E) work across research has developed over the last 20 years from being regarded as, ‘a nice idea to do if possible’, to a funding requirement for key funders. There is increasing evidence that health research conducted with PPI(E) achieves better outcomes for patients.

The [Medical Science Division's PPI Guidance for Researchers](https://oxfordbrc.nihr.ac.uk/ppi/ppi-researcher-guidance/) is a detailed five-part guide covering definitions, academic literature requirements, strategies and best practice, involving diverse groups, the research cycle, recruitment, communication, and more. For individualised support, contact WIN's Public Engagement team ([Carinne Piekema](https://www.win.ox.ac.uk/people/carinne-piekema) / [Hanna Smyth](https://www.win.ox.ac.uk/people/hanna-smyth)). WIN's PPI(E) Lead, who works closely with Carinne and Hanna, is [Sana Suri](https://www.win.ox.ac.uk/people/sana-suri).


## Ethics
You do not need to have ethical approval in place before you do your WIP (although it is needed before you can start your study)

If you have any questions about ethics please contact Nancy (Nancy.Rawlings@ndcn.ox.ac.uk).

Pilot data can sometimes be acquired prior to obtaining your own ethical approval by making use of technical development SOPs. Please contact the radiographers to find out more (radiographers@win.ox.ac.uk)

## Update this guidance

If you notice something in this guidance which doesn't work for you, or sport a typo, please let us know!

You can report any issues with this guidance by [adding a new issue to the GitLab repository](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/issues/2)
