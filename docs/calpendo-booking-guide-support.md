---
layout: default
title: Where to get help
parent: Calpendo booking guide
has_children: false
nav_order: 4
---

# Where to get help with booking using Calpendo
{: .fs-9 }

Get in touch if you have any problems!
{: .fs-6 .fw-300 }

---


## Contact

If you have any questions about this process or your Calpendo booking, please contact one of the WIN Wednesday administrators below:

- WIP Coordinator: cassandra.gouldvanpraag@psych.ox.ac.uk
- WIN Wednesday Seminar Coordinator: andrew.galloway@ndcn.ox.ac.uk

## Update this guidance

If you notice something in this guidance which doesn't work for you, or spot a typo, please let us know!

You can report any issues with this guidance by [adding a new issue to the GitLab repository](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/issues/1)
